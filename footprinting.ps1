#Auteur : Hugo JOYET
#Script pour obtenir des informations sur l'OS

$namePC = hostname
$env:HostIP = ( `
    Get-NetIPConfiguration | `
    Where-Object { `
        $_.IPv4DefaultGateway -ne $null `
        -and `
        $_.NetAdapter.Status -ne "Disconnected" `
    } `
).IPv4Address.IPAddress
$os = (Get-WmiObject -class Win32_OperatingSystem).Caption
$versionOS=(Get-WmiObject Win32_OperatingSystem).Version
$osAJour = [Environment]::OSVersion.Version -ge (new-object 'Version')          
$RamMax=(Get-WmiObject -Class Win32_ComputerSystem ).TotalPhysicalMemory/1GB
$RamDispo=(Get-WmiObject -Class Win32_OperatingSystem).FreePhysicalMemory/1MB
$RamUtilise=$RamMax - $RamDispo
$DisqueDispo=(Get-wmiobject  Win32_LogicalDisk).Freespace/1GB
$DisqueUtilise=(Get-wmiobject  Win32_LogicalDisk).Size/1GB - $DisqueDispo

""

"Name : $namePC"
"IP Principale : $env:HostIP"
"OS : $os"
"Version : $versionOS"
net statistics workstation | Find  "depuis "
"OS a jour ? : $osAJour"
"Ram disponible : $RamDispo GB"
"Ram utilisee : $RamUtilise GB"
"Espace de disque disponible : $DisqueDispo GB"
"Espace de disque utilise : $DisqueUtilise GB"


""

(Get-LocalUser).Name


""

ping 8.8.8.8 | find " Moyenne "

