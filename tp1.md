# Compte Rendu TP1

# I. Self-footprinting

## Host OS

Principales informations de la machine : 
- Commande : `systeminfo`
        
        Nom de l’hôte: LAPTOP-KE2OS454
        Nom du système d’exploitation: Microsoft Windows 10 Famille
        Version du système: 10.0.18363 N/A version 18363
        Type du système: x64-based PC

Quantité de la RAM :
- Commande : `Get-WMIObject win32_physicalmemory`
        
        Capacity: 4294967296

Modèle de la RAM et fabriquant :
- Commande : `Get-CimInstance win32_physicalmemory | Format-Table Manufacturer, PartNumber`
        
        Samsung M471A5244CB0-CTD

## Devices

Marque et modèle du processeur :

- Commande : `Get-WMIObject win32_Processor`
        
        Name: Intel(R) Core(TM) i5-10210U CPU @ 1.60GHz

Nombre de processeurs et de coeurs :

- Commande : `WMIC CPU Get NumberOfCores,NumberOfLogicalProcessors`
        
        NumberOfCores 4
        NumberOfLogicalProcessors 8

Mon precesseur a donc 4 coeurs et 8 processeurs

Ce processeur Intel en est un *i5* de 10ème génération et le *U* à la fin signifie `Mobile power efficient`

Marque et modèle de la Carte Graphique :

- Commande : `Get-WmiObject Win32_VideoController | Select description,driverversion`
        
        description Intel(R) UHD Graphics
        driverversion 26.20.100.7870

Disque Dur :

Marque et modèle : 

- Commande : `Get-disk | fl`
        
        Model : INTEL SSDPEKNW512G8

- Commande: `Get-Partition | fl`
    
        PartitionNumber      : 1
        Size                 : 260 MB
        Type                 : System

        PartitionNumber      : 2
        Size                 : 16 MB
        Type                 : Reserved

        PartitionNumber      : 3
        Size                 : 475.11 GB
        Type                 : Basic

        PartitionNumber      : 4
        Size                 : 1.56 GB
        Type                 : Recovery

La partition 1 qui est la partition System est la partition d'amorçage, elle permet de démarrer le système d'exploitation de l'ordinateur.
La partition Reserved donc la 2 est généralement créé pendant le processus d'installation de Windows, juste avant que le programme d'installation n'alloue de l'espace pour la partition système principale. 
La partition Basic est une partition de type générique pouvant être formatée en interne avec différents modes de formatage. 
La partition Recovery est un outil de logiciel avancé, destiné à tous les utilisateurs qui ont besoin de récupérer des données ou des partitions perdues.

## Users

- Commande : `wmic useraccount list full`

        Name=Administrateur
        Name=DefaultAccount
        Name=hugoj
        Name=Invité
        Name=WDAGUtilityAccount

Le premier User est le compte grand administrateur intégré qui est désactivé par défaut c'est l'User Administrateur 

## Processus

- Commande : `ps`

        4389       0      208       3044                 4   0 System
        274      12     2648       7692               656   1 winlogon
        780      54    32408      37464             12200   0 SearchIndexer
        2965     111    85272     141680     228,94  12848   1 explorer
        289      27     7496      13584       1,13  16616   1 dllhost

**System**
La plupart des threads du mode noyau fonctionnent en tant que processus System.

**Winlogon.exe**
Il s'agit du processus responsable de gérer l'ouverture et la fermeture de session.
Winlongon est actif uniquement lorsque l'utilisateur appuie sur CTRL+ALT+DEL, à ce moment il affiche la boite de sécurité.

**searchindexer**
Le processus searchindexer correspond au service de recherche (ou service d'indexation) de Windows. Il s'agit d'un service chargé d'explorer le contenu du disque dur afin de faciliter la recherche d'un fichier sur ce dernier.

**explorer**
L'explorateur Windows est un élément essentiel puisqu'il régit le bureau de Windows (Shell) avec le menu démarrer, la barre des tâches et le bureau avec le fond d'écran et les raccourcis en icônes.

**dllhost**
dllhost est un processus du support de Microsoft Distributed composant Object Model , partie du système d'architecture des versions modernes de Windows et également quelques programmes faits par d'autres compagnies. En temps normal on l'utilise pour gérer les composants de programme de la Bibliothèque de liens dynamiques. C'est considéré comme le Principal système d'opération composant.

Les processus lancés par l'utilisateur qui est full admin sont ceux avec un SI = 0


## Network

Liste des cartes réseaux : 

- Commande : `Get-NetAdapter | fl name`


        name : Wi-Fi
        name : Connexion réseau Bluetooth

La carte wifi est une norme de communication permettant la transmission de données numériques sans fil. Elle est appelée carte réseau compatible avec la norme WI-FI et équipée d’une antenne émettrice / réceptrice .Elle est également appelée Network Interface Card ( NIC ). Elle constitue l’Interface entre l’ordinateur et le câble du réseau. Sa fonction est de préparer , d’envoyer et de contrôler les données sur le réseau.

Liste des ports TCP et UDP en Utilisation :

- Commande : `netstat -ano -b -p tcp`

```
  Proto  Adresse locale         Adresse distante       État
  TCP    0.0.0.0:135            0.0.0.0:0              LISTENING       1228
  RpcSs
 [svchost.exe]
  TCP    0.0.0.0:445            0.0.0.0:0              LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:808            0.0.0.0:0              LISTENING       5312
 [OneApp.IGCC.WinService.exe]
  TCP    0.0.0.0:5040           0.0.0.0:0              LISTENING       5404
  CDPSvc
 [svchost.exe]
  TCP    0.0.0.0:6646           0.0.0.0:0              LISTENING       5820
 [MMSSHOST.EXE]
  TCP    0.0.0.0:7680           0.0.0.0:0              LISTENING       1156
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:9001           0.0.0.0:0              LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:27036          0.0.0.0:0              LISTENING       12900
 [Steam.exe]
  TCP    0.0.0.0:49664          0.0.0.0:0              LISTENING       84
 [lsass.exe]
  TCP    0.0.0.0:49665          0.0.0.0:0              LISTENING       944
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:49666          0.0.0.0:0              LISTENING       1884
  Schedule
 [svchost.exe]
  TCP    0.0.0.0:49667          0.0.0.0:0              LISTENING       2748
  EventLog
 [svchost.exe]
  TCP    0.0.0.0:49668          0.0.0.0:0              LISTENING       4272
 [spoolsv.exe]
  TCP    0.0.0.0:49680          0.0.0.0:0              LISTENING       100
 Impossible d’obtenir les informations de propriétaire
  TCP    10.33.0.169:139        0.0.0.0:0              LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    10.33.0.169:49423      40.67.251.132:443      ESTABLISHED     5924
  WpnService
 [svchost.exe]
  TCP    10.33.0.169:60741      40.67.254.36:443       ESTABLISHED     11324
 [OneDrive.exe]
  TCP    10.33.0.169:61424      52.7.0.233:443         ESTABLISHED     12720
 [EpicGamesLauncher.exe]
  TCP    10.33.0.169:61425      162.159.133.234:443    ESTABLISHED     12648
 [Discord.exe]
  TCP    10.33.0.169:61645      155.133.248.38:27029   ESTABLISHED     12900
 [Steam.exe]
  TCP    10.33.0.169:61980      51.104.167.245:443     TIME_WAIT       0
  TCP    10.33.0.169:61983      162.159.138.232:443    ESTABLISHED     12648
 [Discord.exe]
  TCP    10.33.0.169:61984      162.159.130.233:443    ESTABLISHED     12648
 [Discord.exe]
  TCP    10.33.0.169:61987      52.2.89.95:443         CLOSE_WAIT      12720
 [EpicGamesLauncher.exe]
  TCP    10.33.0.169:61993      35.190.80.1:443        ESTABLISHED     12648
 [Discord.exe]
  TCP    10.33.0.169:61995      204.79.197.200:443     ESTABLISHED     14312
 [SearchUI.exe]
  TCP    10.33.0.169:61999      161.69.165.56:443      TIME_WAIT       0
  TCP    10.33.0.169:62000      13.107.6.158:443       ESTABLISHED     14312
 [SearchUI.exe]
  TCP    10.33.0.169:62001      13.107.18.11:443       ESTABLISHED     14312
 [SearchUI.exe]
  TCP    10.33.0.169:62003      13.107.4.254:443       ESTABLISHED     14312
 [SearchUI.exe]
  TCP    10.33.0.169:62004      13.107.246.10:443      ESTABLISHED     14312
 [SearchUI.exe]
  TCP    10.33.0.169:62005      152.199.19.161:443     ESTABLISHED     14312
 [SearchUI.exe]
  TCP    10.33.0.169:62006      204.79.197.222:443     ESTABLISHED     14312
 [SearchUI.exe]
  TCP    127.0.0.1:5354         0.0.0.0:0              LISTENING       4596
 [mDNSResponder.exe]
  TCP    127.0.0.1:5354         127.0.0.1:49765        ESTABLISHED     4596
 [mDNSResponder.exe]
  TCP    127.0.0.1:5354         127.0.0.1:49766        ESTABLISHED     4596
 [mDNSResponder.exe]
  TCP    127.0.0.1:6463         0.0.0.0:0              LISTENING       13200
 [Discord.exe]
  TCP    127.0.0.1:27015        0.0.0.0:0              LISTENING       15216
 [AppleMobileDeviceProcess.exe]
  TCP    127.0.0.1:27060        0.0.0.0:0              LISTENING       12900
 [Steam.exe]
  TCP    127.0.0.1:49765        127.0.0.1:5354         ESTABLISHED     15216
 [AppleMobileDeviceProcess.exe]
  TCP    127.0.0.1:49766        127.0.0.1:5354         ESTABLISHED     15216
 [AppleMobileDeviceProcess.exe]
```

- Commande : `netstat -ano -b -p udp `

```
  Proto  Adresse locale         Adresse distante       État
  UDP    0.0.0.0:500            *:*                                    5328
  IKEEXT
 [svchost.exe]
  UDP    0.0.0.0:4500           *:*                                    5328
  IKEEXT
 [svchost.exe]
  UDP    0.0.0.0:5050           *:*                                    5404
  CDPSvc
 [svchost.exe]
  UDP    0.0.0.0:5353           *:*                                    2704
  Dnscache
 [svchost.exe]
  UDP    0.0.0.0:5355           *:*                                    2704
  Dnscache
 [svchost.exe]
  UDP    0.0.0.0:6646           *:*                                    5820
 [MMSSHOST.EXE]
  UDP    0.0.0.0:27036          *:*                                    12900
 [Steam.exe]
  UDP    0.0.0.0:49664          *:*                                    4596
 [mDNSResponder.exe]
  UDP    10.33.0.169:137        *:*                                    4
 Impossible d’obtenir les informations de propriétaire
  UDP    10.33.0.169:138        *:*                                    4
 Impossible d’obtenir les informations de propriétaire
  UDP    10.33.0.169:1900       *:*                                    17756
  SSDPSRV
 [svchost.exe]
  UDP    10.33.0.169:5353       *:*                                    4596
 [mDNSResponder.exe]
  UDP    10.33.0.169:57570      *:*                                    17756
  SSDPSRV
 [svchost.exe]
  UDP    127.0.0.1:1900         *:*                                    17756
  SSDPSRV
 [svchost.exe]
  UDP    127.0.0.1:49666        *:*                                    6208
  iphlpsvc
 [svchost.exe]
  UDP    127.0.0.1:56249        *:*                                    2868
  NlaSvc
 [svchost.exe]
  UDP    127.0.0.1:57571        *:*                                    17756
  SSDPSRV
 [svchost.exe]
  UDP    127.0.0.1:63767        *:*                                    15216
 [AppleMobileDeviceProcess.exe]
  UDP    127.0.0.1:63768        *:*                                    15216
 [AppleMobileDeviceProcess.exe]
 ```

 # II. Scripting

 Voir le script dans le répo.

 # III. Gestion de softs

 Un gestionnaire de paquets est un système qui permet d'installer des logiciels, de les maintenir à jour et de les désinstaller.

Commande pour lister tous les paquets installés :
- Commande : `choco list -l`

        Chocolatey v0.10.15
        chocolatey 0.10.15
        1 packages installed

# IV. Machine virtuelle

```
PS C:\Windows\system32> ssh hjoyet@192.168.120.50
hjoyet@192.168.120.50's password:
Last login: Mon Nov  9 17:08:17 2020
[hjoyet@tp1ws ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:52:fc:77 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global noprefixroute dynamic enp0s3
       valid_lft 86052sec preferred_lft 86052sec
    inet6 fe80::a722:3f3b:91a6:31c9/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:e9:2e:f2 brd ff:ff:ff:ff:ff:ff
    inet 192.168.120.50/24 brd 192.168.120.255 scope global noprefixroute dynamic enp0s8
       valid_lft 407sec preferred_lft 407sec
    inet6 fe80::80db:7ee0:310f:bf07/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

## Partage de fichiers


